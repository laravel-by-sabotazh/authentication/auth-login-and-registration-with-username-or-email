## Introduction

In this project, I will share how to implement Laravel 9 custom auth login and registration with username or email.

### Tutorial

From [codeanddeploy.com](https://codeanddeploy.com/blog/laravel/laravel-8-authentication-login-and-registration-with-username-or-email)

### License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
